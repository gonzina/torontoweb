import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import { FirebaseAppProvider } from "reactfire";
import firebaseConfig from "./firebaseConfig";
import { ThemeProvider } from "@material-ui/styles";

import App from "./App";
import "./index.css";
import theme from "./theme";
import { Suspense } from "react";

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <FirebaseAppProvider firebaseConfig={firebaseConfig}>
        <Suspense fallback={<p>Cargando...</p>}>
          <App />
        </Suspense>
      </FirebaseAppProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
