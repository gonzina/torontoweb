import {createMuiTheme} from "@material-ui/core/styles";

const theme = createMuiTheme({
palette:{
    type:"dark",
    primary:{
        light: "#eee8aa",
        main: "#daa520",
        dark: "#b8860b",
    },
},
});

export default  theme;