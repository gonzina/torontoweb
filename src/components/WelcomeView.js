import { Typography } from "@material-ui/core";
import React from "react";

function WelcomeView(props) {
  return (
    <div>
      <Typography variant="h3" color="initial">
        Bienvenido a Toronto Web
      </Typography>
      <Typography variant="h6" color="primary">
        Selecciona una de las opciones de la izquierda para comenzar
      </Typography>
    </div>
  );
}

export default WelcomeView;
