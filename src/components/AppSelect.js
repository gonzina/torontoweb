import React from 'react'
import { FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	formControl: {
		minWidth: 120,
	},
	selectEmpty: {
			marginTop: theme.spacing(2),
		},
}));

const AppSelect = ({ label, state, onChange, items, disabledItemsId=[] }) => {
	const classes = useStyles();

	return(
		<FormControl variant="filled" className={classes.formControl}>
			<InputLabel id="demo-simple-select-filled-label">{label}</InputLabel>
			<Select
				labelId="demo-simple-select-filled-label"
				id="demo-simple-select-filled"
				value={state}
				onChange={onChange}
			>
				<MenuItem value="">
					<em>Sin seleccionar</em>
				</MenuItem>
					{items.map((item) => item.name ?
						<MenuItem disabled={disabledItemsId.includes(item.id)} value={item.id}>{item.name}</MenuItem> :
						<MenuItem disabled={disabledItemsId.includes(item.id)} value={item}>{item}</MenuItem>    
				)}
			</Select>
		</FormControl>
    );
}

export default AppSelect;
