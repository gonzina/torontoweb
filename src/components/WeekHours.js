import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from '@material-ui/core';

import AppCard from './AppCard';
import ButtonDialog from './ButtonDialog';
  
  function WeekHours(props) {
    const [workDays, setWorkDays] = useState([])

    useEffect(() => {
        props.barber && setWorkDays(props.barber.workDays)
    }, [props.barber])

    const handleChange = (day,index) => {
        const newState=workDays;
        newState.map(item=>{
            item.day===day && console.log(item);
            return item.day===day && item.workHours.splice(index,1);
        })
        setWorkDays(newState);
    }

    return (
        <>
        <ButtonDialog/>

        <TableContainer >
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Lunes</TableCell>
                        <TableCell>Martes</TableCell>
                        <TableCell>Miércoles</TableCell>
                        <TableCell>Jueves</TableCell>
                        <TableCell>Viernes</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        {workDays && workDays.map((days)=>
                            <TableCell>
                                {days.workHours.map((day,index)=>
                                    <AppCard
                                    index={index}
                                    startHour={day[0]}
                                    endHour={day[1]}
                                    handleChange={()=>handleChange(days.day, index)}
                                    />
                                    )}
                            </TableCell>
                        )}
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
        </>
    );
}

export default WeekHours;