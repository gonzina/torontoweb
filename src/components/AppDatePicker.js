import React from 'react';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import "moment/locale/es";
import { getDisabledDates } from '../helpers/dates';

const AppDatePicker = ({
  date,
  onChange,
}) => {
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <DatePicker
            disableToolbar
            disablePast
            inputVariant="filled"
            variant="inline"
            format="DD/MM/YYYY"
            id="date-picker-inline"
            label="Selecciona fecha"
            value={date}
            onChange={onChange}
            shouldDisableDate={getDisabledDates}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        </MuiPickersUtilsProvider>
    );
}

export default AppDatePicker;
