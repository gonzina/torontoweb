import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import AppSelect from "./AppSelect";
import hours from "../data/hours";

const days=["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]

function ButtonDialog(props) {
    const [day,setDay] = useState("");
    const [hourFrom, setHourFrom] = useState("")
    const [hourTo, setHourTo] = useState("")
    const [open, setOpen] = useState(false);
    
    const handleDay = (event) => {setDay(event.target.value)}
    const handleHourFrom = (event) => {setHourFrom(event.target.value)}
    const handleHourTo = (event) => {setHourTo(event.target.value)}
    const handleClickOpen = () => {setOpen(true)}
    const handleClose = () => {setOpen(false)}
    const handleSave = () => {props.handleChange(day,hourFrom,hourTo); setOpen(false);}

    const hoursTo = () => {
        let array = [];
        hours.map(({ h })=>
            hourFrom<h && array.push(h)
        )
        return array;
    }
    const hours2=hoursTo();
    
    
    return (
        <div>
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
            Agregar Horario
        </Button>
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Nuevo Horario"}</DialogTitle>
            <DialogContent>
            <DialogContentText id="alert-dialog-description">
                <AppSelect 
                    label="Día" 
                    items={days} 
                    state={day} 
                    onChange={handleDay} 
                />
                <AppSelect 
                    label="Desde" 
                    items={hours.map(({ h, ms}) => ({name: h, id: ms}))} 
                    state={hourFrom} 
                    onChange={handleHourFrom} 
                />
                <AppSelect 
                    label="Hasta" 
                    items={hours2} 
                    state={hourTo} 
                    onChange={handleHourTo} 
                />
            </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} color="inherit">Cancelar</Button>
            <Button onClick={handleSave}  color="primary" autoFocus>Guardar</Button>
            </DialogActions>
        </Dialog>
        </div>
    );
}

export default ButtonDialog;
