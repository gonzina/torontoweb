import React from 'react';
import {Table,TableBody,TableCell,TableContainer,TableHead,TableRow,Paper, IconButton, Tooltip} from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';

import appointments from '../data/appointments';
import barbers from '../data/barbers';
import hours from '../data/hours';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  tableRightBorder: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderColor: 'inherit',
    borderStyle: 'solid',
  },
});

function AppTable(props) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table" size="small">
        <TableHead>
          <TableRow>
            <StyledTableCell className={classes.tableRightBorder}>Hora</StyledTableCell>
            {barbers.map((barber)=>
              <StyledTableCell className={classes.tableRightBorder} align="left" colSpan={2}>Barbero: {barber.name}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
        {hours.map(({ h: hour })=>
          <StyledTableRow>
            <StyledTableCell className={classes.tableRightBorder}>{hour}</StyledTableCell>
            {barbers.map((barber)=>{
              const row=appointments.find(item => item.hour===hour && item.barber===barber.name);
              return(
                row ?
                <>
                <StyledTableCell align="left">
                  {/* <p>{row.barber}</p> */}
                  <p>{row.user}</p>
                  <p>{row.service}</p>
                </StyledTableCell>
                <StyledTableCell className={classes.tableRightBorder}>
                  <Tooltip title="Confirmar">
                    <IconButton aria-label="Confirmar"><CheckIcon/></IconButton>
                  </Tooltip>
                  <Tooltip title="Cancelar">
                    <IconButton aria-label="Cancelar" ><ClearIcon/></IconButton>
                  </Tooltip>
                </StyledTableCell>
               </>
               :
               <>
               <StyledTableCell/>
               <StyledTableCell className={classes.tableRightBorder}/>
               </>
              );
            })}
          </StyledTableRow>
        )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}


export default AppTable;
