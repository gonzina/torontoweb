import React, { useState } from "react";
import { Button, TextField, Typography } from "@material-ui/core";

function LoginView({ handleSubmit }) {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const handleClick = () => {
    handleSubmit(user, password);
  };

  const handleUser = (event) => {
    setUser(event.target.value);
  };
  const handlePassword = (event) => {
    setPassword(event.target.value);
  };
  return (
    <div>
      <Typography variant="h5" color="primary">
        Inicia Sesión
      </Typography>
      <br />

      {/* <form> */}
      <form onSubmit={handleClick}>
        <TextField
          fullWidth
          label="Usuario"
          variant="outlined"
          value={user}
          onChange={handleUser}
        />
        <br />
        <br />
        <TextField
          fullWidth
          label="Contraseña"
          variant="outlined"
          type="password"
          value={password}
          onChange={handlePassword}
        />
        <br />
        <br />
        <Button
          fullWidth
          // type="submit"
          variant="contained"
          color="primary"
          size="medium"
          onClick={handleClick}
        >
          Login
        </Button>
      </form>
    </div>
  );
}

export default LoginView;
