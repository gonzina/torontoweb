import React from 'react';
import { Card, CardActions, CardContent, IconButton, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles({
    cardStyle:{
        marginBottom:10,
    },
  });

function AppCard(props) {
    const classes = useStyles();
    return (
        <Card className={classes.cardStyle}>
            <CardContent>
                <Typography variant="caption" color="initial">{props.startHour}</Typography>
                <br/>
                <Typography variant="caption" color="initial">{props.endHour}</Typography>
            </CardContent>
            <CardActions>
                <IconButton aria-label="Eliminar" onClick={props.handleChange}>
                    <DeleteIcon fontSize="small"/>
                </IconButton>
            </CardActions>
        </Card>
    );
}

export default AppCard;