import React from 'react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';

import barbers from '../data/barbers';
import barberstats from '../data/barberstats';

function AppStatsTable(props) {
    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Barbero</TableCell>
                        <TableCell align="right">Cabello</TableCell>
                        <TableCell align="right">Barba</TableCell>
                        <TableCell align="right">Ambas</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {barbers.map((barber)=>{
                        const row=barberstats.find(item => item.month===props.month && item.barber.id===barber.id);
                        return(
                            (props.barber===barber.id || props.barber==="") &&
                                <TableRow>
                                    <TableCell>{barber.name}</TableCell>
                                    <TableCell align="right">{row? row.hair : "Sin datos"}</TableCell>
                                    <TableCell align="right">{row? row.beard: "Sin datos"}</TableCell>
                                    <TableCell align="right">{row? row.both : "Sin datos"}</TableCell>
                                </TableRow>
                            )
                    })}    
                </TableBody>
            </Table>

        </TableContainer>
    );
}

export default AppStatsTable;