import React, { useEffect, useState } from 'react';
import { Button, Card, CardContent, CardMedia, Grid, TextField } from '@material-ui/core';

function PersonalData(props) {
    const [name,setName] = useState("");
    const handleName   = (event) => {setName(event.target.value)}

    useEffect(() => {
        props.barber && setName(props.barber.name)
    }, [props.barber])

    return (
        <div>
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <TextField 
                    label="Nombre" 
                    variant="filled" 
                    value={name} 
                    onChange={handleName}
                />
            </Grid>
            <Grid item xs={12} md={3}>
                <Card>
                    <CardContent>
                        <CardMedia
                            component="img"
                            alt="Imagen"
                            width="150"
                            image={props.barber && props.barber.img}
                            title="Titulo"
                        />
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={12}>
                <Button variant="outlined" color="inherit" component="span">Subir imagen</Button>
            </Grid>
        </Grid>    
        </div>
    );
}

export default PersonalData;