import moment from "moment";

export const getDisabledDates = (date) => date.day()===0;

export const getDateNextWeek = (weekDateNumber, actualDate) => {
  const dateDiff = actualDate.day() - weekDateNumber;
  if(actualDate.day() === 0) return moment(actualDate.add( - dateDiff, 'days'));
  return moment(actualDate.add(7 - dateDiff, 'days'));
}

