import barbers from "./barbers";

const barberstats=[
 {month:1 , barber:barbers[0],hair:34,beard:14,both:23},
 {month:1 , barber:barbers[1],hair:30,beard:15,both:20},
 {month:1 , barber:barbers[2],hair:24,beard:10,both:36},

 {month:2 , barber:barbers[0],hair:20,beard:34,both:15},
 {month:2 , barber:barbers[1],hair:36,beard:20,both:8},
 {month:2 , barber:barbers[2],hair:41,beard:14,both:22},

 {month:3 , barber:barbers[0],hair:12,beard:43,both:24},
 {month:3 , barber:barbers[1],hair:31,beard:36,both:23},
 {month:3 , barber:barbers[2],hair:28,beard:51,both:14},

 {month:4 , barber:barbers[0],hair:34,beard:27,both:22},
 {month:4 , barber:barbers[1],hair:25,beard:14,both:20},
 {month:4 , barber:barbers[2],hair:41,beard:22,both:23},

 {month:7 , barber:barbers[0],hair:31,beard:13,both:44},
 {month:7 , barber:barbers[1],hair:38,beard:18,both:33},
 {month:7 , barber:barbers[2],hair:41,beard:15,both:22},

 {month:8 , barber:barbers[0],hair:45,beard:23,both:41},
 {month:8 , barber:barbers[1],hair:35,beard:33,both:32},
 {month:8 , barber:barbers[2],hair:34,beard:43,both:22},

 {month:9 , barber:barbers[0],hair:34,beard:24,both:29},
 {month:9 , barber:barbers[1],hair:24,beard:17,both:23},
 {month:9 , barber:barbers[2],hair:30,beard:34,both:21},

 {month:10, barber:barbers[0],hair:12,beard:43,both:24},
 {month:10, barber:barbers[1],hair:31,beard:36,both:23},
 {month:10, barber:barbers[2],hair:28,beard:51,both:14},

 {month:11, barber:barbers[0],hair:34,beard:27,both:22},
 {month:11, barber:barbers[1],hair:25,beard:14,both:20},
 {month:11, barber:barbers[2],hair:41,beard:22,both:23},
 

];

export default barberstats;