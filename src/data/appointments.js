import barbers from "./barbers";
import services from "./services";

const appointments= [
    {id:1 , hour:"09:00", barber:barbers[0].name, user:"Luchi Gonzalez",  service:services[0].name},
    {id:2 , hour:"09:00", barber:barbers[1].name, user:"Esteban Len",     service:services[1].name},
    
    {id:3 , hour:"09:30", barber:barbers[0].name, user:"Lea Ferrado",     service:services[0].name},
    {id:4 , hour:"09:30", barber:barbers[1].name, user:"Juan Ferrer",     service:services[2].name},
    {id:5 , hour:"09:30", barber:barbers[2].name, user:"Alan Poato",      service:services[0].name},
    
    {id:6 , hour:"11:00", barber:barbers[1].name, user:"Henry Suarez",    service:services[1].name},
    {id:7 , hour:"11:00", barber:barbers[2].name, user:"Juan Iglesias",   service:services[1].name},
    
    {id:8 , hour:"15:30", barber:barbers[0].name, user:"Fede Grosso",     service:services[2].name},
    {id:9 , hour:"15:30", barber:barbers[1].name, user:"Mario Rosales",   service:services[2].name},
    {id:10, hour:"15:30", barber:barbers[2].name, user:"Leandro Deharbe", service:services[0].name},
    
    {id:11, hour:"17:00", barber:barbers[2].name, user:"Pablo Bay",       service:services[1].name},
    
    {id:12, hour:"18:30", barber:barbers[0].name, user:"Kevin Dan Lester",service:services[0].name},
];
export default appointments;