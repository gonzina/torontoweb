const services=[
    {
			id: 1,
			name: "Cabello",
			duration: 1800000,// 30 minutos en milisegundos
		},
    {
			id: 2,
			name:"Barba",
			duration: 1800000,// 30 minutos en milisegundos
		},
    {
			id: 3,
			name: "Cabello y Barba",
			duration: 1800000 * 2,// 60 minutos en milisegundos
		},
];

export default services;
