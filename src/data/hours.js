import moment from "moment";

const calculateHours = (startHour, endHour) => {
    let hoursArray = [];
    let millisecondsCounter = (startHour / 100) * 3600000;
    let h = moment(startHour, "hmm");
    const tansformedEndHour = moment(endHour, "hmm");
    while (h < tansformedEndHour) {
        hoursArray.push({
					h: h.format("HH:mm"),
					ms: millisecondsCounter,
        });
				h.add(30, "minutes");
				millisecondsCounter += 1800000;
    }
    return hoursArray;
}

const hours = calculateHours(900, 2100);

export default hours;
