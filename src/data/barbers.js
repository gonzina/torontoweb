import img1 from "../img/barbers/barber_1.jpg";
import img2 from "../img/barbers/barber_2.jpg";
import img3 from "../img/barbers/barber_3.jpg";

const barbers=[
    {id:1, name:"Juan",     img:img1, workDays:[
        {day:1, workHours:[["09:00","12:30"],["15:00","20:00"]]},
        {day:2, workHours:[["09:00","20:00"]]},
        {day:3, workHours:[["09:00","13:30"]]},
        {day:4, workHours:[["09:00","10:00"],["13:00","16:00"],["18:00","20:00"]]},
        {day:5, workHours:[["13:30","20:00"]]},
    ]},
    {id:2, name:"Rodrigo",  img:img2, workDays:[
        {day:1, workHours:[["13:30","20:00"]]},
        {day:2, workHours:[["09:00","13:30"]]},
        {day:3, workHours:[["09:00","12:30"],["15:00","20:00"]]},
        {day:4, workHours:[["09:00","20:00"]]},
        {day:5, workHours:[["09:00","10:00"],["13:00","16:00"],["18:00","20:00"]]},
    ]},
    {id:3, name:"Pedro",    img:img3, workDays:[
        {day:1, workHours:[["09:00","10:00"],["13:00","16:00"],["18:00","20:00"]]},
        {day:2, workHours:[["09:00","12:30"],["15:00","20:00"]]},
        {day:3, workHours:[["13:30","20:00"]]},
        {day:4, workHours:[["09:00","13:30"]]},
        {day:5, workHours:[["09:00","20:00"]]},
    ]},    
];

export default barbers;