import React, { useEffect, useState } from 'react';
import { Button, Grid, TextField, Typography, CircularProgress } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import moment from "moment";
import { makeStyles } from '@material-ui/core/styles';

import { useFirestore } from 'reactfire';
import 'firebase/firestore';
import { getDateNextWeek } from '../../helpers/dates';

import AppSelect from '../../components/AppSelect';
import AppDatePicker from '../../components/AppDatePicker';
import hours from '../../data/hours';
import services from '../../data/services';


const useStyles = makeStyles(() => ({
	circularProgress: {
		margin: 'auto',
  },
  formControl: {
		minWidth: '200px',
	},
  circularProgressContainer: {
    display: 'flex',
  }
}));

const NewAppointment = () => {
  const classes = useStyles();
  const firestore = useFirestore();

  const [barbers, setBarbers] = useState([]);
  const [user,setUser] = useState("");
  const [barber, setBarber] = useState("");
  const [service,setService] = useState("");
  const [date,setDate] = useState(moment().day() === 6 || moment().day() === 0 ? getDateNextWeek(1, moment()) : moment());
  const [hour,setHour] = useState("");
  const [reservedHours, setReservedHours] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [customerNumber, setCustomerNumber] = useState("");
  const [customerDni, setCustomerDni] = useState("");
  const [openAutocompleteList, setOpenAutocompleteList] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    firestore.collection('barberos').get().then(({ docs }) => {
      const storedBarbers = docs.map(barber => {
        const barberData = barber.data();
        return {
          name: barberData.name,
          id: barber.id
        }
      });
      setBarbers(storedBarbers);
    });
    firestore.collection('clientes').get().then(({ docs }) => {
      const storedCustomers = docs.map(customer => {
        const customerData = customer.data();
        return {
          name: `${customerData.nombre} ${customerData.apellido ? customerData.apellido : ''}`,
          dni: customerData.dni,
          phoneNumber: customerData.telefono,
          id: customer.id
        }
      });
      setCustomers([...storedCustomers, {dni: '', name: 'Nuevo Cliente', phoneNumber: '', id: 'pending'}]);
    });
  },[firestore]);

  useEffect(() => {
    firestore.collection('turnos').where('fecha', '==', new Date(date.startOf('day')).getTime()).get()
    .then(({ docs }) => {
      let todayAppointments = [];
      docs.forEach((appointment) => {
        const dataObject = appointment.data();
        if(Number(dataObject.servicio) === 3) {
          todayAppointments = [...todayAppointments, dataObject.hora, dataObject.hora + 1800000];
        } else {
          todayAppointments = [...todayAppointments, dataObject.hora];
        }
      });
      setReservedHours(todayAppointments);
    });
  }, [firestore, date, loading]);



  const handleUser = (event) =>   {setUser(event.target.value)}

  const handleBarber = ({ target: { value } }) => {
    setBarber(value);
  };
  const handleService = ({ target: { value }}) => {setService(value)};
  
  const handleDate = (selectedDate) => {
    setDate(selectedDate);
  };


  const handleHour = ({ target: { value } })=>{
    setHour(value);
  };


  const saveAppoitment = () => {
    setLoading(true);
    const customerObject= customers.find(({dni}) => String(dni) === String(customerDni));
    const customerCompleteName = user.split(' ');
    const customerName = customerCompleteName[0];
    const customerLastName = customerCompleteName[1] ? customerCompleteName.slice(1).join(' ') : '';
    if(customerObject.id === 'pending') {
      firestore.collection('clientes').add({
        nombre: customerName,
        apellido: customerLastName,
        telefono: customerNumber,
        dni: customerDni,
      }).then((result) => {result.get().then(newCustomer => {
        const customerData = newCustomer.data();
        let newCustomerAdded = {...customers};
        newCustomerAdded = {
          ...newCustomerAdded,
          [customers.length - 1]: {
            name: `${customerData.nombre} ${customerData.apellido ? customerData.apellido : ''}`,
            dni: customerData.dni,
            phoneNumber: customerData.telefono,
            id: newCustomer.id
          }
        }
        setCustomers(Object.values(newCustomerAdded));
        firestore.collection('turnos').add({
        barbero: barber,
        fecha: new Date(date.startOf('day')).getTime(),
        hora: hour,
        servicio: service,
        cliente: newCustomer.id,
      }).finally(() => {
        setUser('');
        setCustomerNumber('');
        setCustomerDni('');
        setHour('');
        setLoading(false);
      });
      })});
    }
    else {
      if(user !== customerObject.name || customerNumber !== customerObject.phoneNumber) {
        firestore.collection('clientes').doc(customerObject.id).update({
          telefono: customerNumber,
          apellido: customerLastName,
          nombre: customerName,
        }).then(() => {
          firestore.collection('clientes').get().then(({ docs }) => {
            const storedCustomers = docs.map(customer => {
              const customerData = customer.data();
              return {
                name: `${customerData.nombre} ${customerData.apellido ? customerData.apellido : ''}`,
                dni: customerData.dni,
                phoneNumber: customerData.telefono,
                id: customer.id
              }
            });
            setCustomers([...storedCustomers, {dni: '', name: 'Nuevo Cliente', phoneNumber: '', id: 'pending'}]);
          });
        });
      }
      firestore.collection('turnos').add({
        barbero: barber,
        fecha: new Date(date.startOf('day')).getTime(),
        hora: hour,
        servicio: service,
        cliente: customerObject.id,
      }).finally(() => {
        setUser('');
        setCustomerNumber('');
        setCustomerDni('');
        setHour('');
        setLoading(false);
      });
    }
  };

  const handleDniSelection = ({target: {innerText}}) => {
    setCustomerDni(String(innerText) || customerDni);
    const customerName = customers.find(({name, phoneNumber, dni}) => {
      if(dni === innerText) {
        return {
          name,
          phoneNumber,
        }
      }
    });
    if(customerName) {
      setUser(customerName.name);
      setCustomerNumber(customerName.phoneNumber);
    }
  };

  const handleDniChange = ({target: {value}}) => {
    setCustomerDni(String(value));
    const customerObject = customers.find(({name, phoneNumber, dni}) => {
      if(dni === value) {
        return {
          name,
          phoneNumber,
        }
      }
    });
    if(!customerObject) {
      let newCustomerAdded = {...customers};
      newCustomerAdded = {...newCustomerAdded, [customers.length - 1]: {...newCustomerAdded[customers.length - 1], dni: value}}
      setCustomers(Object.values(newCustomerAdded));
      setUser(customers[customers.length - 1].name);
      setCustomerNumber(customers[customers.length - 1].phoneNumber);
    }
    else {
      setUser(customerObject.name);
      setCustomerNumber(customerObject.phoneNumber);
    }
  };

  const handleNumberChange = ({target: {value}}) => {
    setCustomerNumber(value);
  }


  const disabledSaveButton = !customerDni || !barber || !service || !date || !hour;

  return(

    <div>
      {loading
        ? 
        <Grid container alignItems="center" alignContent="center"  >
          <Grid item xs={12} className={classes.circularProgressContainer}>
            <CircularProgress size={50} className={classes.circularProgress}/>
          </Grid>
        </Grid>
       
        : (
          <>
          <Typography variant="h5" color="primary">NUEVO TURNO</Typography>
          <Grid container direction="column" spacing={1}>
            <Grid item xs={12}>
              <Grid container direction="row" spacing={1}>
                <Grid item>
                  <Autocomplete
                    id="combo-box-demo"
                    debug
                    className={classes.formControl}
                    open={openAutocompleteList}
                    options={customers}
                    onFocus={() => setOpenAutocompleteList(true)}
                    onBlur={() => setOpenAutocompleteList(false)}
                    onChange={handleDniSelection}
                    getOptionLabel={(option) => option.dni}
                    renderInput={(params) =>
                      <TextField
                        {...params}
                        onChange={handleDniChange}
                        label="DNI"
                        variant="outlined" 
                      />}
                  />
                </Grid>
                <Grid item>
                  <TextField label="Cliente" variant="filled" value={user} onChange={handleUser}/>
                </Grid>
                <Grid item>
                  <TextField label="Teléfono" variant="filled" value={customerNumber} onChange={handleNumberChange}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Grid container direction="row" spacing={1}>
              <Grid item>
                <AppSelect
                  label="Barbero"
                  items={barbers}
                  state={barber}
                  onChange={handleBarber}
                />
              </Grid>
              <Grid item>
                <AppSelect
                  label="Servicio"
                  items={services}
                  state={service}
                  onChange={handleService}
                />
              </Grid>
              <Grid item>
                <AppDatePicker date={date} onChange={handleDate}/>
              </Grid>
              <Grid item>
                <AppSelect
                  label="Horario"
                  items={hours.map(({ h, ms}) => ({name: h, id: ms}))}
                  disabledItemsId={reservedHours}
                  state={hour}
                  onChange={handleHour}
                />
              </Grid>
              </Grid>
            </Grid>
            
            
            <Grid item xs={12}>
              <Button
                variant="contained"
                size="large"
                color="primary"
                disabled={disabledSaveButton}
                onClick={saveAppoitment}
                >Confirmar Turno
              </Button>
            </Grid>
          </Grid>
        </>
      )}
    </div>
  );
}

export default NewAppointment;
