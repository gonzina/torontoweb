import React ,{ useEffect, useState } from 'react';
import { Typography } from '@material-ui/core';
// import { useFirebaseApp } from "reactfire";
// import firebase from "firebase/app";
// import 'firebase/firestore';
// import firebaseConfig from './../firebaseConfig';
import { useFirestore } from 'reactfire';
import 'firebase/firestore';
import moment from "moment";
import { getDateNextWeek } from '../helpers/dates';

import AppTable from '../components/AppTable';
import AppDatePicker from '../components/AppDatePicker';
import { Button } from '@material-ui/core';

// const db = firebase.initializeApp(firebaseConfig);

function TodayAppointments(props) {
    
    let turnosBarbero = [];
    let turnosPorBarbero = [];
    // const firebase = useFirebaseApp();
    const [turnos, setTurnos] = useState([]);
    const firestore = useFirestore();
    const [date,setDate] = useState(moment().day() === 6 || moment().day() === 0 ? getDateNextWeek(1, moment()) : moment());
    const [barbers, setBarbers] = useState([]);
    // let barber = 'probando' 


    const handleDate = (selectedDate) => {
        setDate(selectedDate);
      };


    // const getTurnos = async (fecha) => {
    //     console.log("entro");
    //     await db.firestore().collection('turnos')
    //     .where('fecha', '==', fecha)
    //     .get()
    //     .then(querySnapshot => {
    //       querySnapshot.forEach(documentSnapshot => {
    //         // console.log('turno: ', documentSnapshot.id, documentSnapshot.data());
    //         var barbero = documentSnapshot.data().barbero.name;
    //         var fecha = documentSnapshot.data().fecha.day + '/' + documentSnapshot.data().fecha.month + '/' + documentSnapshot.data().fecha.year;
    //         var hora = documentSnapshot.data().hora;
    //         var pelo = documentSnapshot.data().pelo;
    //         var barba = documentSnapshot.data().barba;
    //         var tipo;
    //         console.log("sigue");
    //         if(pelo && !barba)
    //           tipo = 'Pelo';
    //         if(pelo && barba)
    //           tipo = 'Pelo y Barba'; 
    //         if(!pelo && barba)
    //           tipo = 'Barba';
              
    //         turno = {
    //           fecha : fecha,
    //           hora : hora,
    //           barbero : barbero,
    //           tipo : tipo
    //         }
    //         respuesta.push(turno);
            
    //       });
    //       console.log(respuesta);
    //     });
    //     setTurnos(...turnos, respuesta);
    // }

    
    
    //   useEffect(() => {
    //     getTurnos(fechaConsulta);
    
    //   }, []);
    
    // const buscarBarbero = (barber) => {
    //     barbers.forEach(barb => {
    //         if(barb.id === barber){
    //             barber = 'prueba'
    //             console.log(barber); 

    //         }
    //     });
    // }

    const buscarTurnos = () => {
        const aux = new Date(date.startOf('day')).getTime()
        firestore.collection('turnos')
        .where('fecha', '==', aux)
        .get()
        .then(({ docs }) => {
            const storedTurnos = docs.map(turno => {
                const turnoData = turno.data();
                return {
                  fecha: turnoData.fecha,
                  hora: turnoData.hora,
                  servicio: turnoData.servicio,
                  barbero: turnoData.barbero,
                  cliente: turnoData.cliente
                }
              });
            setTurnos(storedTurnos);
            


            



            
        });

        console.log(turnos.length);
        turnos.forEach(turn => {
            console.log(turn)
        });

        
    }
    
    useEffect(() => {
        firestore.collection('barberos').get().then(({ docs }) => {
          const storedBarbers = docs.map(barber => {
            const barberData = barber.data();
            return {
              name: barberData.name,
              id: barber.id
            }
          });
          setBarbers(storedBarbers);
        });
        
        buscarTurnos();

      },[firestore]);
    
    // useEffect(() => {
        
    //         turnos.forEach(turn => {
    //             if(turn.cliente){ 
    //                 firestore.collection('clientes').doc(turn.cliente).get().then( cliente => {
                    
    //                     turn.cliente = cliente.data().nombre;
                        
    //                 });
                    
    //             }
                
                
    //         });
    //         console.log('turnos',turnos);
      
    // }, [turnos])

    useEffect(() => {





        turnos.forEach(turn => {
            if(turn.cliente){ 
                firestore.collection('clientes').doc(turn.cliente).get().then( cliente => {
                
                    turn.cliente = cliente.data().nombre;
                    
                
                
                
                
                
                    if(turnos.length != 0){
                        barbers.forEach(barb => {
                            console.log('id barbero', barb.id)
                            turnos.forEach(turn => {
                                if(barb.id === turn.barbero && turn.cliente){
                                    console.log('id barbero', turn.barbero)
                                    let turnoAux = {
                                        barbero: barb.name,
                                        cliente: turn.cliente,
                                        fecha: turn.fecha,
                                        hora: turn.hora,
                                        servicio: turn.servicio
                                    }
                                    turnosBarbero.push(turnoAux);
                                }
                            });
                            turnosPorBarbero.push(turnosBarbero);
                            turnosBarbero = [];
                        });
                        console.log('turnosPorBarbero',turnosPorBarbero);
                    }
                
                
                
                
                
                
                });
                
            }
            
            
        });






        
        
        

    }, [barbers, turnos])
    
    return (
        <div>
            <Typography variant="h5" color="primary">TURNOS</Typography>
            
            <AppDatePicker date={date} onChange={handleDate}/>
            <Button
                variant="contained"
                size="large"
                color="primary"
                onClick={buscarTurnos}
                >Buscar Turnos
              </Button>
            <br/>
            <br/>
            <AppTable/>
        </div>
    );
}

export default TodayAppointments;