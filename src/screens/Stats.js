import React, { useState } from 'react';

import AppSelect from '../components/AppSelect';
import barbers from '../data/barbers';
import AppStatsTable from '../components/AppStatsTable';

const months=[
    {id:1 , name:"Enero"}, 
    {id:2 , name:"Febrero"}, 
    {id:3 , name:"Marzo"}, 
    {id:4 , name:"Abril"}, 
    {id:5 , name:"Mayo"}, 
    {id:6 , name:"Junio"}, 
    {id:7 , name:"Julio"}, 
    {id:8 , name:"Agosto"}, 
    {id:9 , name:"Septiembre"}, 
    {id:10, name:"Octubre"}, 
    {id:11, name:"Noviembre"}, 
    {id:12, name:"Diciembre"},
]

function Stats(props) {
    const [barber,setBarber] = useState("");
    const [month,setMonth] = useState("");
    

    const handleBarber = (event) => {setBarber(event.target.value)};
    const handleMonth = (event) => {setMonth(event.target.value)};

    return (
        <div>
            <AppSelect label="Barbero" items={barbers} state={barber} onChange={handleBarber}/>
            <AppSelect label="Mes" items={months} state={month} onChange={handleMonth}/>
            <br/>
            <br/>
            <AppStatsTable month={month} barber={barber}/>
        </div>
    );
}

export default Stats;