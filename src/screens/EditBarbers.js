import React, { useEffect, useState } from 'react';
import { Collapse, Divider, Grid, Typography } from '@material-ui/core';

import AppSelect from '../components/AppSelect';
import PersonalData from '../components/PersonalData';
import WeekHours from '../components/WeekHours';
import barbers from '../data/barbers';


function EditBarbers(props) {
    const [barberId,setBarberId] = useState("");
    const [barber,setBarber] = useState([]);

    const handleBarber = (event) => {setBarberId(event.target.value)}

    useEffect(()=>{
        const found=barbers.find(item=>item.id===barberId);
        setBarber(found);
        // barber && setName(barber.name);
    },[barberId,barber]);

    const Title = (data)=>(
        <>
            <br/>
            <Divider/>
            <Typography variant="overline" color="initial">{data.name}</Typography>
            <br/>
        </>
    )
        
    return (
        <Grid container>
            <Grid item xs={12}>
                <AppSelect label="Barbero" items={barbers} state={barberId} onChange={handleBarber} nullSelect/>
            </Grid>
            <Grid item xs={12}>
                {barber &&
                    <Collapse in={barberId}>
                        <Title name="Datos Personales"/>
                        <PersonalData barber={barber}/>
                        
                        <Title name="Horarios"/>
                        <WeekHours barber={barber}/>
                    </Collapse>
                }
            </Grid>
        </Grid>
    );
}

export default EditBarbers;
