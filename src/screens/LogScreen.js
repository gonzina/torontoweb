import { Card, CardContent, Grid } from "@material-ui/core";
import React from "react";
import LoginView from "../components/LoginView";

import backgroundImage from "../img/wallpaper.jpg";

const styles = {
  root: {
    backgroundImage: `url(${backgroundImage})`,
    height: "100vh",
    width: "100%",
    backgroundSize: "cover",
    display: "flex",
    backgroundPosition: "center",
  },
};

function LogScreen({ handleSubmit }) {

  return (
    <div style={styles.root}>
      <Grid container justify="center" alignItems="center">
        <Grid item md={4}>
          <Card variant="outlined">
            <CardContent>
              <LoginView handleSubmit={handleSubmit} />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default LogScreen;
