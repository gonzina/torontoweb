import React from "react";
import "firebase/auth";
import { useFirebaseApp, useUser } from "reactfire";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomeIcon from "@material-ui/icons/Home";
import AddIcon from "@material-ui/icons/Add";
import TodayIcon from "@material-ui/icons/Today";
// Icono de vista de barberos comentada
// import RecentActorsIcon from '@material-ui/icons/RecentActors';
import EqualizerIcon from "@material-ui/icons/Equalizer";

import Home from "./screens/Home";
import AppDrawer from "./components/AppDrawer";
import NewAppointment from "./screens/new-appointment/NewAppointment";
import AllAppointments from "./screens/AllAppointments";
// Vista no utilizada
// import EditBarbers from './screens/EditBarbers';
import Stats from "./screens/Stats";
import LogScreen from "./screens/LogScreen";

const listItems = [
  {
    text: "Home",
    icon: <HomeIcon />,
    link: "/",
  },
  {
    text: "Agregar Turno",
    icon: <AddIcon />,
    link: "nuevoturno",
  },
  {
    text: "Turnos",
    icon: <TodayIcon />,
    link: "turnos",
  },
  { text: "divider" },
  // La vista de barberos no se utiliza, así que queda comentada
  // {
  //   text:"Editar barberos",
  //   icon:<RecentActorsIcon/>,
  //   link:"barberos"
  // },
  {
    text: "Estadísticas",
    icon: <EqualizerIcon />,
    link: "estadisticas",
  },
];

function App() {
  const firebase = useFirebaseApp();
  const user = useUser();
  // const [loggedIn, setLoggedIn] = useState(false);

  const logout = async () => {
    await firebase.auth().signOut();
  };
  const login = async (email, pass) => {
    await firebase.auth().signInWithEmailAndPassword(email, pass);
  };

  // const handleSubmit = (isLoggedIn) => {
  //   setLoggedIn(isLoggedIn);
  //   // console.log(isLoggedIn);
  // };
  return (
    <div>
      {user.data ? (
        <Router>
          <AppDrawer
            // title="TORONTO WEB"
            title={user.status}
            items={listItems}
            handleSubmit={logout}
          >
            <Switch>
              <Route exact path="/" render={(props) => <Home {...props} />} />
              <Route
                exact
                path="/nuevoturno"
                render={(props) => <NewAppointment {...props} />}
              />
              <Route
                exact
                path="/turnos"
                render={(props) => <AllAppointments {...props} />}
              />
              {/* La vista de barberos no será utilizada de momento, así que queda comentada */}
              {/* <Route exact path="/barberos" render={props=><EditBarbers{...props}/>}/>       */}
              <Route
                exact
                path="/estadisticas"
                render={(props) => <Stats {...props} />}
              />
            </Switch>
          </AppDrawer>
        </Router>
      ) : (
        <LogScreen handleSubmit={login} />
      )}
    </div>
  );
}

export default App;
